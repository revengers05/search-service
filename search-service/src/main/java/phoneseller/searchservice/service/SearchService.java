package phoneseller.searchservice.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import phoneseller.searchservice.entity.ProductEntity;
import phoneseller.searchservice.model.ProductModel;
import phoneseller.searchservice.repository.ISearchRepository;

import java.util.List;

@Slf4j
@Service
public class SearchService {
    @Autowired
    ISearchRepository searchRepository;
    public List<ProductEntity> searchProducts(List<ProductModel> products, String query) {
        for(ProductModel productModel:products){
            ProductEntity product = new ProductEntity();

            product.setId(productModel.getId());
            product.setProductName(productModel.getProductName());
            product.setImageUrl(productModel.getImageUrl());
            product.setColor(productModel.getColor());
            product.setRam(productModel.getRam());
            product.setUsp(productModel.getUsp());
            product.setQuantitySold(productModel.getQuantitySold());
            product.setTotalStock(productModel.getTotalStock());
            searchRepository.save(product);
        }
        return searchRepository.findByProductNameOrColorOrRamOrUspFuzzy(query);
    }
}
