package phoneseller.searchservice.repository;

import org.springframework.data.elasticsearch.annotations.Query;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;
import phoneseller.searchservice.entity.ProductEntity;

import java.util.List;

@Repository
public interface ISearchRepository extends ElasticsearchRepository<ProductEntity, String> {
    @Query("{ \"multi_match\": { \"fields\":  [ \"productName\",\"color\",\"ram\", \"usp\"], \"query\": \"?0\", \"fuzziness\": \"AUTO\"}}}")
    List<ProductEntity> findByProductNameOrColorOrRamOrUspFuzzy(String query);

}
