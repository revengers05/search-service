package phoneseller.searchservice.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import phoneseller.searchservice.entity.ProductEntity;
import phoneseller.searchservice.model.ProductModel;
import phoneseller.searchservice.service.SearchService;

import java.util.List;
import java.util.Objects;

@Slf4j
@RestController
@RequestMapping("/v1.0/search")
public class SearchController {
    @Autowired
    SearchService searchService;

    RestTemplate restTemplate = new RestTemplate();
    @GetMapping("/products")
    public ResponseEntity<List<ProductEntity>> searchProducts(
            @RequestParam String query
    ) {
        log.info("Got call to search product: {}", query);
        String words[] = query.split(" ");
        ResponseEntity<List<ProductModel>> responseEntity = restTemplate.exchange(
                "http://localhost:8081/product-service/v1.0/product/getByName?productName={productName}",
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<>() {
                },
                words[0]
        );
        log.info("ProductModel list is: {}", responseEntity);
        List<ProductModel> products = responseEntity.getBody();

        System.out.println("Fetched " + products);

        if(Objects.nonNull(products)){
            List<ProductEntity> productList = searchService.searchProducts(products, query);
            log.info("productList is: {}", productList);
            return new ResponseEntity<>(productList, HttpStatus.OK);
        }
       return null;
    }

}
